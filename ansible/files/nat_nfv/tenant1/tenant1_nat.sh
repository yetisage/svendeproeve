#!/bin/bash

NAME="ten1-NAT"
PROVIDER_BRIDGE="ovs-br0"
TENANT_BRIDGE="tenant1-bridge"
NAT_OUT_IP="172.16.0.101/17"
DEFAULT_GATEWAY="172.16.127.254"
NAT_IN_IP="192.168.0.254/24"
NAT_OUT_EXT="ten1-nat-out"
NAT_OUT_INT="ten1-nat-intOUT"
NAT_IN_EXT="ten1-nat-in"
NAT_IN_INT="ten1-nat-intIN"
STATE=$1


if [ -z $STATE ]; then
    echo "Missing an argument!";
    exit 1;
fi

if [ $STATE = "start" ]; then

    if [ -z $(sudo ip netns list | grep $NAME | awk '{print $1}') ] && [ ! -d "/sys/class/net/$NAT_OUT_EXT" ]; then
        echo "$NAME namespace and veth peer port is missing. Creating....";
        sudo ip netns add $NAME;
        sudo ip link add $NAT_OUT_EXT type veth peer name $NAT_OUT_INT netns $NAME;
        sudo ip link add $NAT_IN_EXT type veth peer name $NAT_IN_INT netns $NAME;
        sudo ip netns exec $NAME ip link set lo up;
        echo "$NAME namespace and veth peer ports created!";

        echo "Adding veth port to provider bridge";
        sudo ovs-vsctl --may-exist add-port $PROVIDER_BRIDGE $NAT_OUT_EXT;

        echo "Adding veth port to tenant bridge";
        sudo ovs-vsctl --may-exist add-port $TENANT_BRIDGE $NAT_IN_EXT;

        echo "Setting veth pair links to up....";
        sudo ip link set $NAT_OUT_EXT up;
        sudo ip link set $NAT_IN_EXT up;
        sudo ip netns exec $NAME ip link set $NAT_IN_INT up;
        sudo ip netns exec $NAME ip link set $NAT_OUT_INT up;
        echo "links are up!";

        echo "Adding IPs to veth pair....";
        sudo ip netns exec $NAME ip address add $NAT_OUT_IP dev $NAT_OUT_INT;
        sudo ip netns exec $NAME ip address add $NAT_IN_IP dev $NAT_IN_INT;
        echo "IPs added!";

        echo "Adding default route to $NAME namespace";
        sudo ip netns exec $NAME ip route add default via $DEFAULT_GATEWAY;

        echo "Adding iptables rules to namespace"
        ip netns exec $NAME iptables -t nat -A POSTROUTING -o $NAT_OUT_INT -j MASQUERADE;
        ip netns exec $NAME iptables -A FORWARD -i $NAT_IN_INT -o $NAT_OUT_INT -m state --state RELATED,ESTABLISHED -j ACCEPT;
        ip netns exec $NAME iptables -A FORWARD -i $NAT_OUT_INT -o $NAT_IN_INT -j ACCEPT;
        # Blocking the VM from reaching the host subnet.
        ip netns exec $NAME iptables -A FORWARD -i $NAT_IN_INT -d 172.16.0.0/12 -j DROP;
        
        while :
		do
			sleep 30;
		done
		exit 0;
    else
        echo "Seems like the namespace already exists. Exiting....";
        exit 1;
    fi
elif [ $STATE = "stop" ]; then
    if [ -n $(sudo ip netns list | grep $NAME | awk '{print $1}') ]; then
        echo "Killing processes in namespace, to avoid orphaned processes"
        for pids in $(sudo ip netns pids $NAME)
		do
			echo -e "Killing process $pids - $(ps -p $pids -o args | awk 'FNR == 2 {print}')"
			sudo kill $pids > /dev/null;
		done
        echo "Deleting namespace..."
        sudo ip netns del $NAME;
        echo "Namespace deleted!"
        echo "Checking for leftover interfaces..."
        if [ -d "/sys/class/net/$NAT_OUT_EXT" ]; then
            sudo ip link del $NAT_OUT_EXT;

        elif [ -d "/sys/class/net/$NAT_IN_EXT" ]; then
            sudo ip link del $NAT_IN_EXT;
        fi
        echo "Namespace and interfaces deleted!";
        exit 0;
    else
        echo "Seems like the namespace has already been deleted. Checking for leftover interfaces...."
        if [ -d "/sys/class/net/$NAT_OUT_EXT" ]; then
            sudo ip link del $NAT_OUT_EXT;

        elif [ -d "/sys/class/net/$NAT_IN_EXT" ]; then
            sudo ip link del $NAT_IN_EXT;
        else
            echo "No interfaces found!"
        fi
        echo "Interfaces deleted!"
    fi
    echo "OVS bridge interfaces has been left behind INTENTIONALLY! If they need to be removed do so manually."
    exit 0;
fi  
