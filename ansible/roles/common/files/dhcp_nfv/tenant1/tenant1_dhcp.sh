#!/bin/bash

# This script creates a dhcp process and adds it to the tenant1 network namespace

NAME="ten1-dhcp"
BRIDGE_NAME="tenant1-bridge"
STATE=$1
DHCP_RANGE="192.168.0.1,192.168.0.250,255.255.255.0"
DHCP_SERVER_IP="192.168.0.251/24"
TENANT_DEFAULT_GW="192.168.0.254"
DNS_SERVERS="1.1.1.1,8.8.8.8"
if [ -z $STATE ]; then
	echo "Missing an argument!";
	exit 1;
fi
if [ $STATE = "start" ]; then
	
	if [ -z $(sudo ip netns list | grep $NAME | awk '{print $1}') ] && [ ! -d "/sys/class/net/eth0-$NAME" ]; then
		echo "$NAME namespace doesn't exist. Creating...";
		sudo ip netns add $NAME;
		echo "Adding eth0-$NAME with veth0-$NAME peer";
		sudo ip link add dev eth0-$NAME type veth peer name veth0-$NAME netns $NAME;
		echo "Setting eth0-$NAME to up"
		sudo ip link set dev eth0-$NAME up;
		echo "Setting lo to up in namespace $NAME";
		sudo ip netns exec $NAME ip link set lo up;
		echo "Flushing IP on dev veth0-$NAME in namespace $NAME"
		sudo ip netns exec $NAME ip addr add 0.0.0.0 dev veth0-$NAME;
		echo "Setting link veth0-$NAME in namespace $NAME to up"
		sudo ip netns exec $NAME ip link set veth0-$NAME up;
		echo "Starting dnsmasq DHCP server with range $DHCP_RANGE on inteface veth0-$NAME";
		sudo ip netns exec $NAME dnsmasq --dhcp-range=$DHCP_RANGE --dhcp-option=3,$TENANT_DEFAULT_GW --dhcp-option=6,$DNS_SERVERS --interface veth0-$NAME;
		echo "Giving veth0-$NAME the following IP: $DHCP_SERVER_IP";
		sudo ip netns exec $NAME ip addr add $DHCP_SERVER_IP dev veth0-$NAME;

		if [ ! -d "/sys/class/net/$BRIDGE_NAME" ]; then
		echo "$BRIDGE_NAME is missing! Creating...";
		sudo ovs-vsctl --may-exist add-br $BRIDGE_NAME -- add-port $BRIDGE_NAME eth0-$NAME;
		echo "Bridge created and tenant port added!"
		else
		echo "$BRIDGE_NAME exists! Attempting to add tenant port..."
		sudo ovs-vsctl --may-exist add-port $BRIDGE_NAME eth0-$NAME;
		echo "Tenant port added!"
		fi

		while :
		do
			sleep 30;
		done
		exit 0;
	else
		echo "Seems like the namespace already exists. Exiting....";
		exit 1;
	fi
fi
if [ $STATE = "stop" ]; then
	if [ -n $(sudo ip netns list | grep $NAME | awk '{print $1}') ] && [ -d "/sys/class/net/eth0-$NAME" ]; then
		echo "Detecting PIDs in namespace $NAME...."
		for pids in $(sudo ip netns pids $NAME)
		do
			echo -e "Killing process $pids - $(ps -p $pids -o args | awk 'FNR == 2 {print}')"
			sudo kill $pids > /dev/null;
		done
		echo "Deleting veth eth0-$NAME";
		sudo ip link delete eth0-$NAME;
		echo "Deleting namespace $NAME";
		sudo ip netns delete $NAME;
		exit 0;
	elif [ -z $(sudo ip netns list | grep $NAME | awk '{print $1}') ] && [ -d "/sys/class/net/eth0-$NAME" ]; then
		echo "Namespace $NAME doesn't exist, but the veth interface still does. Destroying veth interface....";
		sudo ip link delete eth0-$NAME;

		echo "Since the veth interface was left behind from the namespace, orphaned processes might be left behind too. Please check for any left over processes"
		exit 0;
	else
		echo "The namespace $NAME doesn't exist and no interfaces appear to have been left behind. Exiting....";
		exit 1;
	fi
fi
#for tenant in $(sudo ip netns list | grep tenant1 | awk '{print $1}')
#do
#	echo "test $tenant";
#	exit 0;
#done
