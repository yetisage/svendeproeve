#!/bin/bash
VM_NAME=""
VM_RAM=512
VMDISK_PATH=""
VMDISK_SIZE=10
VM_CPUS=1
usage() {
	echo "Usage: $0 [OPTION]...
Creates a Ubuntu Server 20.04 LTS virtual machine.

OPTIONS:

 -n, --name		The name of the virtual machine
 -m, --memory		The amount of memory to give the virtual machine. Defaults to 512 MB
 -d, --disk		The path of the disk image. This includes the filename. Defaults to the current directory
 -s, --size		The size of the disk image. Defaults to 10 GB
 -c, --cpus		The amount of vcpus to give the virtual machine. Defaults to 1
 "
}
#usage
while getopts 'n:m:d:s:c:' option
do
	case $option in
		n) VM_NAME=${OPTARG} ;;
		m) VM_RAM=${OPTARG} ;;
		d) VMDISK_PATH=${OPTARG} ;;
		s) VMDISK_SIZE=${OPTARG} ;;
		c) VM_CPUS=${OPTARG} ;;
		h) usage ;;
		[?]) usage; exit 1 ;;
	esac
done
declare -a vararray=("$VM_NAME" $VM_RAM $VMDISK_PATH $VMDISK_SIZE $VM_CPUS)

i=0
for options in ${vararray[@]}
do
	let i++;
	case $options in
		-?)
			echo "$0: an argument appears to be missing a value and has tried to pass another argument instead"
			echo "$0: VALUE: $options"
			exit 1
			;;
	esac
done

if [ ! -n "$VM_NAME" ]; then
	echo "
$0: invalid value for option '-n'
	
Try '-h' for more information."
	exit 1
fi

if [ ! -n "$VMDISK_PATH" ]; then
	echo "$0: missing path for VM disk image. Defaulting to current directory..."
	VMDISK_PATH="./${VM_NAME}_image.qcow2"
fi

if [ -d "$VMDISK_PATH" ]; then
	echo "$0: VM disk path appears to be a directory. Defaulting file name to \$VM_NAME_image.qcow2..."
	VMDISK_PATH="${VM_NAME}_image.qcow2"
fi
#echo "VM_NAME=$VM_NAME
#VM_RAM=$VM_RAM
#VMDISK_PATH=$VMDISK_PATH
#VMDISK_SIZE=$VMDISK_SIZE
#VM_CPUS=$VM_CPUS
#"
virt-install \
--name $VM_NAME \
--ram $VM_RAM \
--disk path=$VMDISK_PATH,size=$VMDISK_SIZE \
--vcpus $VM_CPUS \
--os-type linux \
--os-variant ubuntu20.04 \
--network network:br-int \
--graphics none \
--console pty,target_type=serial \
--location 'http://archive.ubuntu.com/ubuntu/dists/focal/main/installer-amd64/' \
--extra-args 'console=ttyS0,115200n8 serial' \

exit 0
